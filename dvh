#!/usr/bin/env bash

if [ $# -eq 0 ]
then
    echo "Project name required."
    exit
fi

source "./projects/$1/.env"

# Project
PROJECT_DIR="/var/www/${PROJECT_NAME}"
PROJECT_ROOT_DIR="${PROJECT_DIR}/public"

# Docker
DOCKER_PROJECT_NAME="wpthco"
NETWORK_NAME="${DOCKER_PROJECT_NAME}_default"
DOCKER_PHPFPM="${DOCKER_PROJECT_NAME}_php-fpm_1"

# Enable project site on Apache
# Enable mod_rewrite
docker exec ${DOCKER_PROJECT_NAME}_apache_1 bash -c "a2ensite $PROJECT_NAME; a2enmod rewrite; service apache2 reload"

# Enable project site on NGINX
docker exec ${DOCKER_PROJECT_NAME}_nginx_1 vhm ensite $PROJECT_NAME

# Create project directory on PHP-FPM
docker exec ${DOCKER_PROJECT_NAME}_php-fpm_1 mkdir -p ${PROJECT_NAME}/public

# Download WordPress core
docker exec \
    -w $PROJECT_ROOT_DIR \
    ${DOCKER_PHPFPM} \
    ""wp core download""

# Create WordPress config file
docker exec \
    -w $PROJECT_ROOT_DIR \
    ${DOCKER_PHPFPM} \
    ""wp config create \
        --dbname="$DB_NAME" \
        --dbuser="$DB_USER" \
        --dbpass="$DB_PASS" \
        --dbhost="$DB_HOST" \
        --dbprefix="$DB_PREFIX"""
    # http://www.wpbeginner.com/wordpress-security
    #   --extra-php <<PHP
#   define( 'DISALLOW_FILE_EDIT', true );
#PHP

# Create database
docker exec \
    -w $PROJECT_ROOT_DIR \
    ${DOCKER_PHPFPM} \
    ""wp db create""

# WordPress core data installation
docker exec \
    -w $PROJECT_ROOT_DIR \
    ${DOCKER_PHPFPM} \
    ""wp core install \
        --url="$WP_URL" \
        --title="$WP_TITLE" \
        --admin_user="$WP_ADMIN_USER" \
        --admin_email="$WP_ADMIN_EMAIL" \
        --admin_password="$WP_ADMIN_PASSWORD"""

# Plugins installation
docker exec \
    -w $PROJECT_ROOT_DIR \
    ${DOCKER_PHPFPM} \
    wp plugin install ${WP_PLUGINS_LIST[@]} \
        --activate
